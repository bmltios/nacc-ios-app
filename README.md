DESCRIPTION
-----------
This is an extremely basic "toy" app that is for use by NA members for
"calculating their cleantime -not their Recovery."
It asks the user to enter their clean date in the app, then displays a
textual breakdown of their cleantime to date, along with a string of keytags.

REQUIREMENTS
------------
This is an iOS app, made for iPhones and iPads. It is only available via the Apple App Store, and requires iOS 9.0 or above.

INSTALLATION
------------

This will be installed via the Apple App Store, and will be a free app.

LICENSE
-------

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BMLT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code.  If not, see <http://www.gnu.org/licenses/>.

CHANGELIST
----------
***Version 3.5.0.1000* ** *- TBD*

- Updated to Swift 5.

***Version 3.4.0.2000* ** *- October 10, 2017*

- Adds a simple Watch app that reports the results on your wrist.

***Version 3.3.0.3000* ** *- September 19, 2017*

- Official release.

***Version 3.3.0.2000* ** *- September 13, 2017*
- Added the two new keytags (5 and 15 years).
- Updated to the latest iOS and Xcode versions. Base version is now 10.0.
- Darkened the text to improve readability.
- First beta release.

***Version 3.2.0.3000* ** *- November 14, 2016*
- This adds a new switch that allows the keytag display to be disabled.

***Version 3.1.0.3000* ** *- September 22, 2016*
- This implements the new Swift 3 language.
- Added the new 10,000 days and 30-year tags.
- It now remembers the last date entered.
- The keychain now scrolls to the end when the result is announced.

***Version 3.0.0.3000* ** *- July 15, 2015*
- This reflects a complete, top-to-bottom rewrite of the app.
- It has been rewritten in Swift, Apple's new programming language.
